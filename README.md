Debian 10 on Dell XPS 13 7390 2-in-1 (2019)
===========================================

This repository contains the scripts and process for getting a (mostly) 
working Debian Buster/Bullseye installation on the new Dell XPS 13 2-in-1. 
Much of this was taken from partipating in [this Reddit thread](https://www.reddit.com/r/Dell/comments/cx0fkc/xps_13_2_in_1_7390_linux_boot_attempt/).

### Not Working

* Webcam (no fix known yet)

## Setup

You'll need to boot from a kernel >=v5.3. If you are using a Debian 10 image 
that probably means you are on v5.2. So press `e` at the GRUB menu, find the 
line that starts with `linux` and append `module_blacklist="intel_lpss_pci"` 
before proceeding to boot.

Once you have your base system installed, boot again using that same flag. 
You'll want to find an elternate means for connecting to the internet since 
your wireless will not work yet. I used an Android phone's USB tethering 
fuctionality.

> If you are using Ubuntu and have Secure Boot enabled, you will need to
> disable it.

Next, clone this repository which contains what you need to:

* Download the mainline kernel (v5.4-rc4 at time of writing)
* Apply patches for booting without blacklisting LPSS, fix the touchscreen, + other
* Compile the custom kernel with the appropriate config
* Backport the wireless drivers and fix bluetooth

First upgrade your system and install some tools.

```
sudo apt update
sudo apt upgrade
sudo apt install git build-essential flex bison libssl-dev libelf-dev bc kernel-package
```

Get this repository.

```
git clone https://gitlab.com/emrose/xps13-7390_debian
cd xps13-7390_debian
```

Run the scripts.

```
./opt/scripts/build-custom-kernel.sh
./opt/scripts/strip-kernel-modules.sh
./opt/scripts/update-initramfs.sh
./opt/scripts/backport-wireless.sh
./opt/scripts/fix-wifi-and-bluetooth.sh
```

Reboot with fingers crossed.

```
sudo reboot
```

Notes
-----

* Some users have reported issues with some HDMI dongles, if that's you [there is a workaround](https://gitlab.com/emrose/xps13-7390_debian/issues/4)
