#!/bin/bash
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
SRCPATH=$SCRIPTPATH/../../linux

git clone https://github.com/torvalds/linux $SRCPATH
cd $SRCPATH
git reset --hard
git checkout v5.4-rc6
cp $SCRIPTPATH/../../opt/kernel.conf $SRCPATH/.config
patch -p1 < $SCRIPTPATH/../patches/lpss.patch
patch -p1 < $SCRIPTPATH/../patches/hid.patch
patch -p1 < $SCRIPTPATH/../patches/0-suspend.patch
patch -p1 < $SCRIPTPATH/../patches/1-suspend.patch
patch -p1 < $SCRIPTPATH/../patches/dellxps-icelake-screencorruptionfix.patch
make -j$(nproc)
make -j$(nproc) modules
fakeroot make-kpkg -j $(nproc) --initrd --append-to-version=xps13 kernel-image kernel-headers
sudo dpkg -i ../*.deb
